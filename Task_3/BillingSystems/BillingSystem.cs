﻿using System;
using System.Collections.Generic;
using System.Linq;
using TelephoneSystem.ATS;
using TelephoneSystem.Events;

namespace TelephoneSystem.BillingSystems
{
    public class BillingSystem
    {        
        private readonly IList<Contract> _abonentArchive;
        private readonly IDictionary<string, IList<Report>> _reportsArchive;
        private IStation _stationsArchive;
        private event EventHandler<CallEventArgs> ReturnPhoneEvent;

        public BillingSystem()
        {
            _abonentArchive = new List<Contract>();
            _reportsArchive = new Dictionary<string, IList<Report>>();
        }

        public void AddStationToArchive(IStation station)
        {
            station.CheckNumberEvent += IsPhoneAvaileble;
            station.GetReportEvent += AddRecordCall;
            _stationsArchive = station;            
            ReturnPhoneEvent += station.OnPhoneStartingCall;
        }

        public void AddAbonentToArchive(Contract contract)
        {
            contract.Phone.StartCallStationEvent += _stationsArchive.IncomingCall;
            _abonentArchive.Add(contract);
        }

        public void IsPhoneAvaileble(object sender, CallEventArgs arg)
        {
            int length = _abonentArchive.Count;
            foreach (var item in _abonentArchive)
            {
                length--;
                if (item.Phone.GetNumber() == arg.PhoneNumber)
                {
                    ReturnPhoneObject(item.Phone, new CallEventArgs() { SourcePhoneNumber = arg.SourcePhoneNumber, PhoneNumber = arg.PhoneNumber });
                    break;
                }
                else if(length == 0)
                {
                    Console.WriteLine("This number is not availeble");
                }               
            }
            
        }

        public void AddRecordCall()
        {
            var stationCall = _stationsArchive.CallInProcess;
            _stationsArchive.CallInProcess = null;
            var callsArchive = new List<Report>();
            foreach (var abonent in _abonentArchive)
            {
                if (abonent.Phone.GetNumber() == stationCall.SourcePhoneNumber)
                {
                    int timecall = (int)stationCall.TimeCall.TotalSeconds;
                    decimal callcost = (decimal)stationCall.TimeCall.TotalSeconds * abonent.Tarif.PriceForSecond;
                    if (_reportsArchive.ContainsKey(stationCall.SourcePhoneNumber))
                    {
                        _reportsArchive[stationCall.SourcePhoneNumber].Add(new Report(stationCall.CallDate.ToString("d"), timecall, callcost, stationCall.PhoneNumber));
                    }
                    else
                    {
                        callsArchive.Add(new Report(stationCall.CallDate.ToString("d"), timecall, callcost, stationCall.PhoneNumber));
                        _reportsArchive.Add(stationCall.SourcePhoneNumber, callsArchive);
                    }
                    break;
                }
            }
        }

        public IList<Report> GetReportCalls(Contract contract)
        {
            return _reportsArchive[contract.Phone.GetNumber()];
        }

        public IEnumerable<Report> GetSortedReportByDate(Contract contract)
        {
            IEnumerable<Report> temp = GetReportCalls(contract);
            return temp.OrderBy(u => u.DateCall);
        }

        public IEnumerable<Report> GetSortedReportBySum(Contract contract)
        {
           IEnumerable<Report> temp = GetReportCalls(contract);
            return temp.OrderBy(u => u.CostCall);
        }

        public IEnumerable<Report> GetSortedReportByAbonent(Contract contract)
        {
           IEnumerable<Report> temp = GetReportCalls(contract);
            return temp.OrderBy(u => u.AbonentNumber);
        }
        protected virtual void ReturnPhoneObject(object sender, CallEventArgs arg)
        {
            ReturnPhoneEvent.Invoke(sender, arg);
        }

    }
}
