﻿using TelephoneSystem.ATS;

namespace TelephoneSystem.BillingSystems
{
    public class Contract 
    {
        public double ContractNumber { get; private set; }
        public IPhone Phone { get; private set; }
        public Tarif Tarif { get; private set; }
        public Contract(double contractNumber)
        {
            ContractNumber = contractNumber;
        }
        public void AddPhoneNumber(IPhone phone)
        {
            Phone = phone;
        }
        public void ChooseTarif(Tarif tarif)
        {
            Tarif = tarif;
        }
    }
}
