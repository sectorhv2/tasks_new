﻿
namespace TelephoneSystem.BillingSystems
{
    public class Tarif
    {
        public decimal PriceForSecond { get; private set; } 
        public Tarif(decimal priceForSecond)
        {
            PriceForSecond = priceForSecond;
        }
    }
}
