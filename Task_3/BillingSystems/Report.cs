﻿namespace TelephoneSystem.BillingSystems
{
    public class Report
    {
        public string DateCall { get; private set; }
        public int CallDuration { get; private set; }
        public decimal CostCall { get; private set; }
        public string AbonentNumber { get; private set; }
        public Report(string dateCall, int durationCall, decimal costCall, string abonentNumber)
        {
            DateCall = dateCall;
            CallDuration = durationCall;
            CostCall = costCall;
            AbonentNumber = abonentNumber;
        }
    }
}
