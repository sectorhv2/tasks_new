﻿using System;
using System.Collections.Generic;
using TelephoneSystem.ATS;
using TelephoneSystem.BillingSystems;

namespace TelephoneSystem
{
    static class Program
    {
        static void Main(string[] args)
        {
            Port port1 = new Port();
            Port port2 = new Port();
            Port port3 = new Port();
            IStation station = new Station()
            {
                PortController = new PortController(new List<Port>(new Port[] { port1, port2, port3 }))
            };
            station.BindPorts();
            BillingSystem billingSystem = new BillingSystem();
            billingSystem.AddStationToArchive(station);                        
            Tarif simple = new Tarif(0.12m);

            var abonent1 = CreateAbonent(1, "+375-29-256-35-67", simple);
            var abonent2 = CreateAbonent(2, "+375-33-789-77-77", simple);
            var abonent3 = CreateAbonent(3, "+375-33-999-99-99", simple);
            var abonent4 = CreateAbonent(4, "+375-33-888-99-88", simple);
            AddAbonents(billingSystem, new List<Contract>(new Contract[] { abonent1, abonent2, abonent3, abonent4 }));

            abonent1.Phone.ConnectPort(port1);
            abonent1.Phone.DisconnectPort();

            abonent1.Phone.Call("+375-33-789-77-77");
            abonent2.Phone.Call("+375-33-999-99-99");
            abonent1.Phone.Call("+375-33-888-99-88");

            Console.WriteLine($"Report for phone {abonent1.Phone.GetNumber()}");            
            GetReport(billingSystem.GetReportCalls(abonent1));
            Console.WriteLine();
            Console.WriteLine($"Sorted report by sum for phone {abonent1.Phone.GetNumber()}");            
            GetReport(billingSystem.GetSortedReportBySum(abonent1));
            Console.ReadLine();
        }

        static void GetReport(IEnumerable<Report> report)
        {
            foreach (var item in report)
            {
                Console.WriteLine($"Date Call: {item.DateCall}, duration: {item.CallDuration} sec, cost: {item.CostCall.ToString("0.00")} dollars, \n Target Phone: {item.AbonentNumber}");
                Console.WriteLine();
            }
        }
        static void AddAbonents(BillingSystem system, List<Contract> abonents)
        {
            foreach (var abonent in abonents)
            {
                system.AddAbonentToArchive(abonent);
            }
        }
        static Contract CreateAbonent(int number, string phoneNumber, Tarif tarif)
        {
            Contract abonent = new Contract(number);
            abonent.AddPhoneNumber(new Phone(phoneNumber));
            abonent.ChooseTarif(tarif);
            return abonent;
        }
    }
}
