﻿using System;
using TelephoneSystem.Events;

namespace TelephoneSystem.ATS
{
    public interface IStation
    {
        CallInformation CallInProcess { get; set; }
        event Action GetReportEvent;
        event EventHandler<CallEventArgs> CheckNumberEvent;
        CallInformation GetCallInformation();
        void BindPorts();        
        void IncomingCall(object sender, CallEventArgs arg);
        void OnPhoneStartingCall(object sender, CallEventArgs arg);
    }
}
