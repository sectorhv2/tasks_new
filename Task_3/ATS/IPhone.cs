﻿using System;
using TelephoneSystem.Events;

namespace TelephoneSystem.ATS
{
    public interface IPhone
    {
        event EventHandler<CallEventArgs> StartCallWithPortEvent;
        event EventHandler<CallEventArgs> StartCallStationEvent;
        event EventHandler<CallEventArgs> AnswerCallEvent;
        event EventHandler<CallEventArgs> EndCallEvent;
        string GetNumber();
        void ConnectPort(Port port);
        void DisconnectPort();
        void Call(string phoneNumber);
        void OnRequest(object sender, CallEventArgs arg);
        void AnswerCall(CallEventArgs args);
        void RefuseCall(CallEventArgs args);
    }
}
