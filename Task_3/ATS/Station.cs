﻿using System;
using System.Linq;
using TelephoneSystem.Events;

namespace TelephoneSystem.ATS
{
    public class Station : IStation
    {
        public CallInformation CallInProcess { get; set; }
        public PortController PortController { get; set; }

        public event Action GetReportEvent;
        public event EventHandler<CallEventArgs> CheckNumberEvent;

        public void BindPorts()
        {
            PortController.Ports.ToList().ForEach(x => x.StartCallEvent += IncomingCall);
            PortController.Ports.ToList().ForEach(x => x.AnswerCallEvent += AnswerCall);
            PortController.Ports.ToList().ForEach(x => x.EndCallEvent += FinishCall);
        }
        public void OnPhoneStartingCall(object sender, CallEventArgs arg)
        {
            var port = PortController.Ports.FirstOrDefault(x => x.ConnectedPhoneNumber == arg.PhoneNumber);
            if (port != null && port.IsConnected)
            {
                port.PhoneCalledByStation(arg.SourcePhoneNumber);
            }
            else if (PortController.Ports != null)
            {
                int length = PortController.Ports.Count();
                foreach (var portNew in PortController.Ports)
                {
                    length--;
                    if (portNew != null && !portNew.IsConnected)
                    {
                        portNew.PhoneCalledByStation(sender, arg.SourcePhoneNumber);
                        break;
                    }
                    else if (length == 0)
                    {
                        Console.WriteLine("No free ports. Try later");
                    }
                }
                
            }
        }

        public void AnswerCall(object sender, CallEventArgs arg) 
        {
            CallInProcess = new CallInformation() { PhoneNumber = arg.PhoneNumber, SourcePhoneNumber = arg.SourcePhoneNumber, CallDate = DateTime.Today , TimeCall = DateTime.Now.TimeOfDay };
        }

        public void FinishCall(object sender, CallEventArgs arg)
        {
            if (CallInProcess != null)
            {
                CallInProcess.TimeCall = (DateTime.Now.TimeOfDay - CallInProcess.TimeCall);
            }
            else
            {
                CallInProcess = new CallInformation() { PhoneNumber = arg.PhoneNumber, SourcePhoneNumber = arg.SourcePhoneNumber, CallDate = DateTime.Today, TimeCall = TimeSpan.Zero };                
            }
            GetReportToBilling();
        }

        public void GetReportToBilling()
        {
            ReportToBilling();
        }

        public void IncomingCall(object sender, CallEventArgs arg)
        {
            IsIncomingNumberAvaileble(this, new CallEventArgs() { PhoneNumber = arg.PhoneNumber, SourcePhoneNumber = arg.SourcePhoneNumber });
        }

        public CallInformation GetCallInformation()
        {
            return CallInProcess;
        }    

        protected virtual void ReportToBilling()
        {
            GetReportEvent?.Invoke();
        }

        protected virtual void IsIncomingNumberAvaileble(object sender, CallEventArgs arg)
        {
            CheckNumberEvent?.Invoke(sender, arg);
        }
    }
}