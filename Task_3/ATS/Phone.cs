﻿using System;
using TelephoneSystem.Events;

namespace TelephoneSystem.ATS
{
    public class Phone : IPhone
    {
        private Port _port;
        private readonly string _number;
        public event EventHandler<CallEventArgs> StartCallWithPortEvent;
        public event EventHandler<CallEventArgs> StartCallStationEvent;
        public event EventHandler<CallEventArgs> EndCallEvent;
        public event EventHandler<CallEventArgs> AnswerCallEvent;
        public Phone(string number)
        {
            _number = number;
        }
        public string GetNumber()
        {
            return _number;
        }
        public void ConnectPort(Port port)
        {
            if (_port == null && !port.IsConnected)
            {
                _port = port;
                _port.IsConnected = true;
                _port.ConnectedPhoneNumber = _number;
                _port.Request += OnRequest;
                StartCallWithPortEvent += _port.OnPhoneStartingCall;
                AnswerCallEvent += _port.OnPhoneAnswerCall;
                EndCallEvent += _port.OnPhoneFinishCall;
            }
        }
        public void DisconnectPort()
        {
            if (_port != null)
            {
                _port.IsConnected = false;
                _port.ConnectedPhoneNumber = null;
                _port.Request -= OnRequest;
                StartCallWithPortEvent -= _port.OnPhoneStartingCall;
                AnswerCallEvent -= _port.OnPhoneAnswerCall;
                EndCallEvent -= _port.OnPhoneFinishCall;
                _port = null;
            }
        }
        public void Call(string phoneNumber)
        {
            if (phoneNumber == _number)
            {
                Console.WriteLine("Wrong number");
            }
            else if (_port != null)
            {
                OnStartPortCall(this, new CallEventArgs() { PhoneNumber = phoneNumber, SourcePhoneNumber = _number });
            }
            else
            {
                OnStartStationCall(this, new CallEventArgs() { PhoneNumber = phoneNumber, SourcePhoneNumber = _number });
            }
        }
        public void OnRequest(object sender, CallEventArgs arg)
        {
            Console.WriteLine($"Incoming call from number: {arg.SourcePhoneNumber} to number {_number}");
            Console.WriteLine("Answer? Y/N");
            char k = Console.ReadKey().KeyChar;
            if (k == 'Y' || k == 'y')
            {
                Console.WriteLine();
                AnswerCall( new CallEventArgs() { SourcePhoneNumber = arg.SourcePhoneNumber, PhoneNumber = _number});
                Console.WriteLine("Talking....");
                while ( k != 'n' && k != 'N')
                {
                    k = Console.ReadKey().KeyChar;
                }
            }

            Console.WriteLine();
            RefuseCall(new CallEventArgs() {  PhoneNumber = _number, SourcePhoneNumber = arg.SourcePhoneNumber });
        }        
        public void AnswerCall(CallEventArgs args)
        {
            OpenCall(this, args);
        }      
        public void RefuseCall(CallEventArgs args)
        {
           EndCall(this, args);
        }        protected virtual void OpenCall(object sender, CallEventArgs arg)
        {
            AnswerCallEvent?.Invoke(sender, arg);
        }
        protected virtual void EndCall(object sender, CallEventArgs arg)
        {            
            EndCallEvent?.Invoke(sender, arg);
        }        protected virtual void OnStartPortCall(object sender, CallEventArgs arg)
        {
            StartCallWithPortEvent?.Invoke(sender, arg);
        }
        protected virtual void OnStartStationCall(object sender, CallEventArgs arg)
        {
            StartCallStationEvent?.Invoke(sender, arg);
        }
    }
}