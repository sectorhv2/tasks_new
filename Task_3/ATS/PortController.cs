﻿using System.Collections.Generic;

namespace TelephoneSystem.ATS
{
    public class PortController
    {
        private readonly ICollection<Port> _ports;
        public IEnumerable<Port> Ports { get { return _ports; } }
        public PortController(ICollection<Port> ports)
        {
            _ports = ports;
        }
    }
}