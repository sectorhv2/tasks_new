﻿using System;
using TelephoneSystem.Events;

namespace TelephoneSystem.ATS
{
    public class Port
    {
        public PortStatus Status { get;private set; }
        public string ConnectedPhoneNumber { get; set; }
        public bool IsConnected { get; set; }

        public event EventHandler<CallEventArgs> StartCallEvent;
        public event EventHandler<CallEventArgs> Request;
        public event EventHandler<CallEventArgs> AnswerCallEvent;
        public event EventHandler<CallEventArgs> EndCallEvent;

        public Port()
        {
            Status = PortStatus.Free;
        }
       
        public void OnPhoneStartingCall(object sender, CallEventArgs arg)
        {
            if (Status == PortStatus.Free)
            {
                Status = PortStatus.Busy;
                OnStartCall(this, arg);
                Status = PortStatus.Free;
            }
        }
        
        public void PhoneCalledByStation(string sourceNumber)
        {
            if (Status == PortStatus.Free)
            {
                Status = PortStatus.Busy;
                OnRequest(this, new CallEventArgs() { SourcePhoneNumber = sourceNumber });
            }
        }

        public void PhoneCalledByStation(object phone, string sourceNumber)
        {
            var telephone = phone as IPhone;
            if (Status == PortStatus.Free && telephone != null)
            {                
                Request += telephone.OnRequest;
                telephone.AnswerCallEvent += OnPhoneAnswerCall;
                telephone.EndCallEvent += OnPhoneFinishCall;
                Status = PortStatus.Busy;
                ConnectedPhoneNumber = sourceNumber;
                IsConnected = true;
                OnRequest(this, new CallEventArgs() { SourcePhoneNumber = sourceNumber });
                GetPortFree(telephone);
            }
        }
        
        public void OnPhoneAnswerCall(object sender, CallEventArgs arg)
        {
            Status = PortStatus.Busy;
            OnFAnswerhCall(this, arg);
        }
        
        public void OnPhoneFinishCall(object sender, CallEventArgs arg)
        {
            Status = PortStatus.Free;
            OnFinishCall(this, arg);
        }
        protected virtual void OnFAnswerhCall(object sender, CallEventArgs arg)
        {
            AnswerCallEvent?.Invoke(sender, arg);
        }
        protected virtual void OnFinishCall(object sender, CallEventArgs arg)
        {
            EndCallEvent?.Invoke(sender, arg);
        }
        protected virtual void OnStartCall(object sender, CallEventArgs arg)
        {
            StartCallEvent?.Invoke(sender, arg);
        }
        protected virtual void OnRequest(object sender, CallEventArgs arg)
        {
            Request?.Invoke(sender, arg);
        }

        private void GetPortFree(IPhone phone)
        {
            Request -= phone.OnRequest;
            phone.AnswerCallEvent -= OnPhoneAnswerCall;
            phone.EndCallEvent -= OnPhoneFinishCall;
            Status = PortStatus.Free;
            ConnectedPhoneNumber = null;
            IsConnected = false;
        }

    }
}