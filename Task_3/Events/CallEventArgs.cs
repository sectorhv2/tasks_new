﻿using System;

namespace TelephoneSystem.Events
{
    public struct CallEventArgs : IEventInformation
    { 
        public string PhoneNumber { get; set; }
        public string SourcePhoneNumber { get; set; }
    }
}
