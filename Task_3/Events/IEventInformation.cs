﻿using System;

namespace TelephoneSystem.Events
{
    interface IEventInformation
    {
        string PhoneNumber { get; set; }
        string SourcePhoneNumber { get; set; }
    }
}
