﻿using System;

namespace TelephoneSystem.Events
{
    public class CallInformation : IEventInformation
    {
        public string PhoneNumber { get; set; }
        public string SourcePhoneNumber { get; set; }
        public TimeSpan TimeCall { get; set; }
        public DateTime CallDate { get; set; }
    }
}
