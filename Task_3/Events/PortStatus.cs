﻿namespace TelephoneSystem.Events
{
    public enum PortStatus
    {
        Free,
        Busy
    }
}