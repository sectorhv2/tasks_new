﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace task_0
{
    public class Player 
    {
        protected IPlayerContent PlayerContent;
        public Player(IPlayerContent playerContent)
        {
            PlayerContent = playerContent;
        }

        public void Play() 
        {
            PlayerContent.Play();
        }
        public void Stop() 
        {
            PlayerContent.Stop();
        }
    }
}