﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace task_0
{
    public class VideoFile: MediaFile
    {
        public int FrameWidth { get; private set; }

        public int FrameHeight { get; private set; }

        public int Lasting { get; private set; }

        public VideoFile (int size, string Name, string Extension, int Last, int Width, int Height) : base(size,  Name, Extension)
        {
            FrameHeight = Height;
            FrameWidth = Width;
            Lasting = Last;
        }

        public override void Play()
        {

        }
        public override void Stop()
        {

        }
    }
}