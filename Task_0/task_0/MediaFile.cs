﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace task_0
{
    public abstract class MediaFile : IPlayerContent
    {
        public string Extension { get; private set; }

        public int Size { get; private set; }

        public string Name { get; private set; }

        protected MediaFile(int size, string Name, string Extension)
        {
            this.Extension = Extension;
            this.Name = Name;
            Size = size;
        }

        public abstract void Play();
        public abstract void Stop();
    }
}