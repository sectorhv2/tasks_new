﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace task_0
{
    public class AudioFile : MediaFile
    {
        public int Length { get;private set; }
        public string Author { get;private set; }
        public string Title { get;private set; }

        public AudioFile(int size, string Name, string Extension, int length, string author, string title) : base(size, Name, Extension)
        {
            Length = length;
            Author = author;
            Title = title;
        }

        public override void Play()
        {
            
        }

        public override void Stop()
        {

        }

    }
}