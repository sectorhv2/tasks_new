﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace task_0
{
    public interface IPlayerContent
    {
        void Play();
        void Stop();
    }
}