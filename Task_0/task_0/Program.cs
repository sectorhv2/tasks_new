﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_0
{
    class Program
    {
        static void Main(string[] args)
        {
            MediaLibary libary = new MediaLibary();
            MediaFile audio1 = new AudioFile(1,"abs","mp3",6, "Bob", "My Song");
            MediaFile audio2 = new AudioFile(5, "new2", "mp3", 4, "San", "His voice");
            PlayList playlist1 = new PlayList();
            playlist1.Add(audio1);
            playlist1.Add(audio2);
        }
    }
}
