﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace task_0
{
    public class PhotoFile : MediaFile
    {
        public string Dimensions { get;private set; }

        public int Width { get; private set; }

        public int Height { get;private set; }

        public PhotoFile(int size, string Name, string Extension, string dimensions, int width, int height) : base(size, Name, Extension)
        {
            Dimensions = dimensions;
            Width = width;
            Height = height;
        }

        public override void Play()
        {

        }
        public override void Stop()
        {

        }
    }
}