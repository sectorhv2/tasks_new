﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace task_0
{
    public class PlayList : IPlayerContent
    {
        public int ID { get; private set; }
        public ICollection<IPlayerContent> MediaFile { get;private set; }
        public PlayList()
        {
            MediaFile = new Collection<IPlayerContent>();
        }

        public void Add(IPlayerContent mediaFile)
        {
            ID++;
            MediaFile.Add(mediaFile);
        }

        public void Clear()
        {
            ID = 0;
            MediaFile.Clear();
        }

        public void Play()
        {

        }

        public void Stop()
        {

        }


    }
}