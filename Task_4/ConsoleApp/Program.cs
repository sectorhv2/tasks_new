﻿using ServiceLayer.Abstraction;
using ServiceLayer;
using System;

namespace ConsoleApp
{
    static class Program
    {
        static void Main(string[] args)
        {
            IAppService appService = new AppService();
            appService.StartApp();
            Console.WriteLine("Programm is started");
            Console.WriteLine("Press any key to finish the programm...");
            Console.ReadLine();
            appService.StopApp();
        }
    }
}
