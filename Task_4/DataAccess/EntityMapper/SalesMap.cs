﻿using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.EntityMapper
{
    public class SalesMap : IEntityTypeConfiguration<Sale>
    {
        public void Configure(EntityTypeBuilder<Sale> builder)
        {
            builder.Property(e => e.Id).ValueGeneratedNever();
            builder.Property(e => e.Date).HasColumnType("date");
            builder.Property(e => e.Sum).HasColumnType("money");
            builder.HasOne(d => d.Client)
                .WithMany(p => p.Sales)
                .HasForeignKey(d => d.ClientId)
                .HasConstraintName("FK_Sales_Clients");
            builder.HasOne(d => d.Commodity)
                .WithMany(p => p.Sales)
                .HasForeignKey(d => d.CommodityId)
                .HasConstraintName("FK_Sales_Goods");
            builder.HasOne(d => d.Manager)
                .WithMany(p => p.Sales)
                .HasForeignKey(d => d.ManagerId)
                .HasConstraintName("FK_Sales_Manager");
            builder.Property(e => e.FileName).HasMaxLength(50);
        }    
    }
}
