﻿using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.EntityMapper
{
    public class ManagersMap : IEntityTypeConfiguration<Manager>
    {
        public void Configure(EntityTypeBuilder<Manager> builder)
        {
            builder.ToTable("Manager");
            builder.Property(e => e.Id).ValueGeneratedNever();
            builder.Property(e => e.SecondName).HasMaxLength(50);
        }
    }
}
