﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DataAccessLayer.Models
{
    public partial class Client
    {
        public Client()
        {
            Sales = new HashSet<Sale>();
        }

        public int Id { get; set; }
        public string Client1 { get; set; }

        public virtual ICollection<Sale> Sales { get; set; }

        public override string ToString()
        {
            return Client1.ToString();
        }
    }
}
