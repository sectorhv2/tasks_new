﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DataAccessLayer.Models
{
    public partial class Manager
    {
        public Manager()
        {
            Sales = new HashSet<Sale>();
        }

        public int Id { get; set; }
        public string SecondName { get; set; }

        public virtual ICollection<Sale> Sales { get; set; }

        public override string ToString()
        {
            return SecondName;
        }
    }
}
