﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DataAccessLayer.Models
{
    public partial class Sale
    {
        public int Id { get; set; }
        public int? ManagerId { get; set; }
        public int? ClientId { get; set; }
        public int? CommodityId { get; set; }
        public decimal? Sum { get; set; }
        public DateTime? Date { get; set; }
        public string FileName { get; set; }

        public virtual Client Client { get; set; }
        public virtual Good Commodity { get; set; }
        public virtual Manager Manager { get; set; }

        public override string ToString()
        {
            return FileName;
        }
    }
}
