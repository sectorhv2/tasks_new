﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DataAccessLayer.Models
{
    public partial class Good
    {
        public Good()
        {
            Sales = new HashSet<Sale>();
        }

        public int Id { get; set; }
        public string Commodity { get; set; }
        public int? Count { get; set; }
        public decimal? Cost { get; set; }

        public virtual ICollection<Sale> Sales { get; set; }

        public override string ToString()
        {
            return Commodity;
        }
    }
}
