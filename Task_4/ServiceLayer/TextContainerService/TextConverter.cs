﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ServiceLayer.FileReaders;

namespace ServiceLayer.TextContainerService
{
    public class TextConverter
    {
        private readonly List<string> _readedFile;
        private readonly string _filePath;
        public string FileName { get; set; }
        public IList<ContainerRow> RowsToAddDBContainer { get; set; }

        public TextConverter(string path)
        {
            var reader = new FileReader();
            _readedFile = reader.ReadFile(path);
            _filePath = path;
            FileName = Path.GetFileName(path);
            RowsToAddDBContainer = GetContainer();
        }

        private ContainerRow GetRow(string manager, string parametrs)
        {
            var containerRow = new ContainerRow();
            containerRow.Manager = manager;
            string[] tempParams = parametrs.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < tempParams.Length; i++)
            {
                try
                {
                    switch (i)
                    {
                        case 0:
                            containerRow.Date = DateTime.Parse(tempParams[i]);
                            break;
                        case 1:
                            containerRow.Clients = tempParams[i];
                            break;
                        case 2:
                            containerRow.Commodity = tempParams[i];
                            break;
                        case 3:
                            containerRow.Sum = Convert.ToDecimal(tempParams[i]);
                            break;
                        default:
                            break;
                    }
                }
                catch (FormatException)
                {
                    throw new FormatException("Wrong input data");
                }
            }
            return containerRow;
        }

        private IList<ContainerRow> GetContainer()
        {
            var result = new List<ContainerRow>();
            string[] words = FileName.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var items in _readedFile)
            {
                result.Add(GetRow(words[0], items));
            }
            return result;
        }

        //List<string> ReadFile(string path)
        //{
        //    try
        //    {
        //        return File.ReadAllLines(path).ToList();                
        //    }
        //    catch (IOException e)
        //    {
        //        throw new IOException(e.Message);
        //    }
        //}
    }
}
