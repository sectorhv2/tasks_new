﻿using System;
using System.Collections.Generic;

namespace ServiceLayer.TextContainerService
{
    public class ContainerRow
    {
        public string Manager { get; set; }
        public DateTime Date { get; set; }
        public string Commodity { get; set; }
        public string Clients { get; set; }
        public decimal Sum { get; set; }

    }
}