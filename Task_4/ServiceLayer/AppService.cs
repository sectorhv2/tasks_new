﻿using Microsoft.Extensions.Configuration;
using ServiceLayer.Abstraction;

namespace ServiceLayer
{
    public class AppService : IAppService
    {
        private Watcher watcher; 
        public IConfigurationRoot AppConfiguration { get; set; }

        public AppService() 
        {
            var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            AppConfiguration = builder.Build();
        }

        public void StartApp()
        {
            var path = AppConfiguration.GetConnectionString("Directory");
            var filter = AppConfiguration.GetConnectionString("DefualtType");
            watcher = new Watcher(path, filter);
            watcher.Watch();
        }

        public void StopApp()
        {
            watcher.StopWatch();
        }
    }
}
