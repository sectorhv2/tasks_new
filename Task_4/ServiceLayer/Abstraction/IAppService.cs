﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLayer.Abstraction
{
    public interface IAppService
    {
        void StartApp();
        void StopApp();
    }
}
