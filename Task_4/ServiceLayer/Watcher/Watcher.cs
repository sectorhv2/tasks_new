﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceLayer
{
    public class Watcher
    {  
        private FileSystemWatcher watcher;
        private BlockingCollection<string> blockingCollection;
        public string Path { get; set; }
        public string Filter { get; set; }


        public Watcher(string path, string filter)
        {
            this.Path = path;
            this.Filter = filter;            
        }

        public void Watch()
        {
            //blockingCollection = new BlockingCollection<string>();
            watcher = new FileSystemWatcher(Path);
            watcher.Created += newThread; /*(o, e) => blockingCollection.Add(e.FullPath);*/
            watcher.Changed += newThread;
            watcher.Filter = Filter;
            watcher.Error += OnError;
            watcher.EnableRaisingEvents = true;
            //Parallel.ForEach(blockingCollection.GetConsumingEnumerable(), newThread);
        }

        public void StopWatch()
        {
            watcher.EnableRaisingEvents = false;
            //blockingCollection.CompleteAdding();
        }

        private void newThread(object sender, FileSystemEventArgs path)
        {
            var service = new DBService();
            Task task = new Task(() => service.CreateContainer(path.FullPath));
            task.Start();
            task.Wait();
            //Thread myThread = new Thread(new ParameterizedThreadStart(service.CreateContainer));
            //myThread.Start(path.FullPath);
        }

        private static void OnError(object sender, ErrorEventArgs e) =>
            throw new Exception(e.GetException().Message);
    }
}
