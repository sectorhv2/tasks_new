﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceLayer.FileReaders
{
    public class FileReader
    {
        public List<string> ReadFile(string path)
        {
            while (true)
            {
                try
                {                    
                    var temp = File.ReadAllLines(path);
                    return temp.ToList();
                }
                catch (IOException)
                {
                    Thread.Sleep(1000);
                }
            }
        }
    }
}
