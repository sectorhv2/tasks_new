﻿using DataAccessLayer.Models;
using EntityProject.Factory;
using RepositoryLayer.Abstractions;
using RepositoryLayer.UnitofWork;
using System.Collections.Generic;
using ServiceLayer.TextContainerService;

namespace ServiceLayer
{
    public class DBService
    {
        static object locker = new object();
        public void CreateContainer(object path)
        {
            var container = new TextConverter((string)path);
            lock (locker)
            {                
                AddDataToDB(container);
            }            
        }

        private void AddDataToDB(TextConverter container)
        {
            using (IUnitofWork unitofWork = new UnitofWork(new ContextFactory().CreateDbContext()))
            {
                if (!IsDataReapated(unitofWork, container.FileName))
                {
                    var _idCollction = new List<int>();
                    int maxID = unitofWork.SalesRepository.GetLastIDSales() + 1;
                    foreach (var datarow in container.RowsToAddDBContainer)
                    {
                        unitofWork.SalesRepository.Insert(CreateUnitToSend(unitofWork, datarow, maxID, container.FileName));
                        maxID++;
                        _idCollction.Add(maxID);
                    }
                    unitofWork.Save();
                }         
                
            }
        }

        private bool IsDataReapated(IUnitofWork unitofWork, string filename)
        {
            var ishave = unitofWork.SalesRepository.GetbyName(filename);
            if (ishave != null)
            {
                return true;
            }
            return false;
        }

        private Sale CreateUnitToSend(IUnitofWork unitofWork, ContainerRow datarow, int maxID, string filename)
        {
            var manager = unitofWork.ManagersRepository.GetbyName(datarow.Manager);
            var commodity = unitofWork.GoodsRepository.GetbyName(datarow.Commodity);
            var client = unitofWork.ClientRepository.GetbyName(datarow.Clients);

            Sale sale = new Sale()
            {
                Id = maxID,
                Date = datarow.Date,
                Commodity = commodity,
                Client = client,
                Manager = manager,
                Sum = datarow.Sum,
                FileName = filename
            };
            return sale;
        }

    }
}
