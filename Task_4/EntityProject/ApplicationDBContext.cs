﻿using DataAccessLayer.EntityMapper;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace EntityProject
{
    public partial class ApplicationDBContext : DbContext
    {
        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Good> Goods { get; set; }
        public virtual DbSet<Manager> Managers { get; set; }
        public virtual DbSet<Sale> Sales { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Cyrillic_General_CI_AS");

            modelBuilder.ApplyConfiguration(new ClientMap());
            modelBuilder.ApplyConfiguration(new GoodsMap());
            modelBuilder.ApplyConfiguration(new ManagersMap());
            modelBuilder.ApplyConfiguration(new SalesMap());
        }        
    }
}
