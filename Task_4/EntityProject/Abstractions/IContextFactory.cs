﻿using EntityProject;
using Microsoft.EntityFrameworkCore.Design;

namespace RepositoryLayer.Abstractions
{
    public interface IContextFactory : IDesignTimeDbContextFactory<ApplicationDBContext>
    {
        ApplicationDBContext CreateDbContext();
    }
}
