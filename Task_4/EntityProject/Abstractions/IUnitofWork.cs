﻿using RepositoryLayer.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryLayer.Abstractions
{
    public interface IUnitofWork : IDisposable
    {
        ClientRepository ClientRepository { get; }
        GoodsRepository GoodsRepository { get; }
        ManagersRepository ManagersRepository { get; }
        SalesRepository SalesRepository { get; }
        void Save();
    }
}
