﻿using EntityProject;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RepositoryLayer.Abstractions
{
    public abstract class GenericRepository<TEntity> where TEntity : class
    {
        internal ApplicationDBContext context;
        internal DbSet<TEntity> dbSet;

        protected GenericRepository(ApplicationDBContext context)
        {
            this.context = context;
            dbSet = context.Set<TEntity>();
        }
        public virtual TEntity GetbyName(string includeProperti)
        {
            IEnumerable<TEntity> query = dbSet;
            TEntity result = null;
            foreach (var quer in query)
            {
                string temp = quer.ToString() ?? string.Empty;
                if (temp.Contains(includeProperti))
                {
                    result = quer;
                    break;
                }
            }
            
            return result;  
        }

        public virtual TEntity GetByID(object id)
        {            
            return dbSet.Find(id);
        }

        public virtual void Insert(TEntity entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Delete(object id)
        {
            TEntity entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        public virtual void Update(TEntity entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return dbSet.ToList();
        }
    }
}
