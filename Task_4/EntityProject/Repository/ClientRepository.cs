﻿using DataAccessLayer.Models;
using EntityProject;
using RepositoryLayer.Abstractions;
using System.Linq;

namespace RepositoryLayer.Repository
{
    public class ClientRepository : GenericRepository<Client>, IRepository<Client>
    {
        public ClientRepository(ApplicationDBContext context): base(context)
        {
        }
    }
}
