﻿using DataAccessLayer.Models;
using EntityProject;
using RepositoryLayer.Abstractions;

namespace RepositoryLayer.Repository
{
    public class ManagersRepository : GenericRepository<Manager>, IRepository<Manager>
    {
        public ManagersRepository(ApplicationDBContext context) : base(context)
        {
        }
    }
}
