﻿using DataAccessLayer.Models;
using EntityProject;
using RepositoryLayer.Abstractions;
using System.Linq;

namespace RepositoryLayer.Repository
{
    public class SalesRepository : GenericRepository<Sale>, IRepository<Sale>
    {
        public SalesRepository(ApplicationDBContext context) : base(context)
        {
        }

        public virtual int GetLastIDSales()
        {
            if (context.Sales.Count() > 0)
            {
                return context.Sales.Select(x => x.Id).Max();
            }
            else
            {
                return 0;
            }            
        }
    }
}
