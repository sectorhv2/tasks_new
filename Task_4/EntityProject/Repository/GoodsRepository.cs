﻿using DataAccessLayer.Models;
using EntityProject;
using RepositoryLayer.Abstractions;

namespace RepositoryLayer.Repository
{
    public class GoodsRepository : GenericRepository<Good>, IRepository<Good>
    {
        public GoodsRepository(ApplicationDBContext context) : base(context)
        {
        }
    }
}
