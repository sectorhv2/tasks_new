﻿using EntityProject;
using RepositoryLayer.Abstractions;
using RepositoryLayer.Repository;
using System;

namespace RepositoryLayer.UnitofWork
{
    public class UnitofWork : IUnitofWork
    {
        private readonly ApplicationDBContext context;
        private ClientRepository clientRepository;
        private GoodsRepository goodsRepository;
        private ManagersRepository managersRepository;
        private SalesRepository salesRepository;

        public UnitofWork(ApplicationDBContext context)
        {
            this.context = context;
        }

        public ClientRepository ClientRepository
        {
            get
            {

                if (this.clientRepository == null)
                {
                    this.clientRepository = new ClientRepository(context);
                }
                return clientRepository;
            }
        }

        public GoodsRepository GoodsRepository
        {
            get
            {

                if (this.goodsRepository == null)
                {
                    this.goodsRepository = new GoodsRepository(context);
                }
                return goodsRepository;
            }
        }

        public ManagersRepository ManagersRepository
        {
            get
            {

                if (this.managersRepository == null)
                {
                    this.managersRepository = new ManagersRepository(context);
                }
                return managersRepository;
            }
        }

        public SalesRepository SalesRepository
        {
            get
            {

                if (this.salesRepository == null)
                {
                    this.salesRepository = new SalesRepository(context);
                }
                return salesRepository;
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
