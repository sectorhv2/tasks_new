﻿using System;

namespace AircraftHeirarchy
{
    public class CargoPlane : CivilPlane
    {
        private readonly double MediumSpeedWithFullCargo;
        public CargoPlane(string model, double range, int crew, double fuel, float carrying, int passengNum, int mediumspeed) : 
            base(model, range, crew, fuel, carrying, passengNum, mediumspeed)
        {
            MediumSpeedWithFullCargo = mediumspeed - (mediumspeed * 0.5);
        }
        public override void Flight()
        {
            Console.WriteLine($"Cargo Plane {Model} flight");
        }
    }
}
