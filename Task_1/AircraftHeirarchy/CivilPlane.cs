﻿using System;

namespace AircraftHeirarchy
{
    public abstract class CivilPlane 
    {
        public int MediumSpeed { get;private set; }
        public double RangeFlight { get;private set; }
        public int CrewNumber { get;private set; }
        public double FuelConsumtion { get;private set; }
        public string Model { get;private set; }
        public float Carrying { get; private set; }
        public int PassengerNumber { get; private set; }

        protected CivilPlane(string model, double range, int crew, double fuel, float carrying, int passengNum, int mediumspeed)
        {
            Model = model;
            RangeFlight = range;
            CrewNumber = crew;
            FuelConsumtion = fuel;
            Carrying = carrying;
            PassengerNumber = passengNum;
            MediumSpeed = mediumspeed;
        }

        public abstract void Flight();
    }
}
