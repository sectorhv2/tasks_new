﻿using System;

namespace AircraftHeirarchy
{
    public class PassengerPlane : CivilPlane
    {
        private readonly double MediumSpeedWithFullPassangers;

        public PassengerPlane(string model, double range, int crew, double fuel, float carrying, int passengNum, int mediumspeed) :
            base(model, range, crew, fuel, carrying, passengNum, mediumspeed)
        {
            MediumSpeedWithFullPassangers = mediumspeed - (mediumspeed * 0.2);
        }
        public override void Flight()
        {
            Console.WriteLine($"Passsanger Plane {Model} flight");
        }
    }
}
