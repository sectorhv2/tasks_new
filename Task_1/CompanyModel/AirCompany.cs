﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using AircraftHeirarchy;

namespace CompanyModel
{
    public class AirCompany : IAirPark
    {
        private ICollection<CivilPlane> Planes { get; set; }

        public readonly string CompanyName;

        public AirCompany(string name)
        {
            Planes = new Collection<CivilPlane>();
            CompanyName = name;
        }
        public void AddPlane(CivilPlane plane)
        {
            Planes.Add(plane);
        }

        public void RemovePlane(CivilPlane plane)
        {
            if (Planes != null)
            {
                Planes.Remove(plane);
            }
        }

        public void SortParkRange()
        {
            var temp = Planes.ToList();
            temp.Sort(delegate (CivilPlane range, CivilPlane rangenext)
            {
                if (range.RangeFlight > rangenext.RangeFlight)
                {
                    return 1;
                }
                else if (range.RangeFlight < rangenext.RangeFlight)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            });
            Planes = temp;            
        }

        public void ShowPark()
        {
            int i = 1;
            foreach (var plane in Planes)
            {
                Console.WriteLine($"{i} - {plane.Model}");
                i++;
            }
        }

        public void ShowFuelConsumtionPlane(double min, double max)
        {
            foreach (var plane in Planes)
            {
                if (plane.FuelConsumtion >= min && plane.FuelConsumtion <= max)
                {
                    Console.WriteLine($"{plane.Model}");
                }
            }

        }

        public ICollection<CivilPlane> ReturnPlanes()
        {
            return Planes;
        }
    }
}
