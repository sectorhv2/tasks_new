﻿using System;
using System.Collections.Generic;
using AircraftHeirarchy;

namespace CompanyModel
{
    public interface IAirPark 
    {
        void AddPlane(CivilPlane plane);
        void RemovePlane(CivilPlane plane);
        void ShowFuelConsumtionPlane(double min, double max);
        void SortParkRange();
        void ShowPark();
        ICollection<CivilPlane> ReturnPlanes();
    }
}
