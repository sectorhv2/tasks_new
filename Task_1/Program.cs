﻿using System;
using AircraftHeirarchy;
using CompanyModel;

namespace AirCompanyTask
{
    static class Program
    {
        static void Main(string[] args)
        {
            AirCompany company1 = new AirCompany("Airobus");
            CivilPlane il76Plane = new CargoPlane("ИЛ-76", 4200, 10, 9.5, 60.5f, 0, 876);
            CivilPlane an225Plane = new CargoPlane("АН-225", 4000, 6, 34.5, 225.500f, 0, 850);
            CivilPlane boeing747100Plane = new PassengerPlane("Boeing 747-100", 7200, 2, 18.2, 5f, 434, 958);
            CivilPlane boeing747400ERPlane = new PassengerPlane("Boeing 747-400ER", 14200, 2, 17.5, 7f, 524, 976);
            CivilPlane boeing7478Plane = new PassengerPlane("Boeing 747-8", 14100, 2, 17.1, 6f, 581, 982);

            company1.AddPlane(il76Plane);
            company1.AddPlane(an225Plane);
            company1.AddPlane(boeing747100Plane);
            company1.AddPlane(boeing747400ERPlane);
            company1.AddPlane(boeing7478Plane);

            Console.WriteLine($"Aviation fleet of the company {company1.CompanyName}:\n");
            company1.ShowPark();
            ShowPassandWeight(company1);
            ShowFuelConsumtion(company1, 18, 19);
            Console.WriteLine($"\nAircraft fleet of the company after sorting: ");
            company1.SortParkRange();
            company1.ShowPark();
            Console.ReadLine();

        }

        static void ShowPassandWeight(AirCompany company)
        {
            var plains= company.ReturnPlanes();
            int passWeight = 0;
            float allCarrying = 0;
            foreach (var plane in plains)
            {
                allCarrying += plane.Carrying;
                passWeight += plane.PassengerNumber;
            }

            Console.WriteLine($"The total capacity of all aircraft of the company: {passWeight} person");
            Console.WriteLine($"The total carrying capacity of all aircraft of the company: {allCarrying} tons");
        }

        static void ShowFuelConsumtion(AirCompany company, int min, int max)
        {
            Console.WriteLine($"\nAircraft according to the given parameters of fuel consumption \n(between {min} and {max} tons per hour):");
            company.ShowFuelConsumtionPlane(min, max);
        }
    }
}
