﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WordsDescription
{
    interface IWord
    {
        string GetWordSelf();
        int GetCount();
        List<int> GetStringsNumber();
    }
}