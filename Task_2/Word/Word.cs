﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WordsDescription
{
    public class Word : IWord
    {
        private string WordSelf { get; set; }
        private int Count { get; set; }
        private List<int> StringNumber { get; set; }

        public Word(string word, int count, List<int> numberstring)
        {
            WordSelf = word;
            Count = count;
            StringNumber = numberstring;
        }

        public string GetWordSelf()
        {
            return WordSelf;
        }

        public int GetCount()
        {
            return Count;
        }

        public List<int> GetStringsNumber()
        {
            return StringNumber;
        }
    }
}