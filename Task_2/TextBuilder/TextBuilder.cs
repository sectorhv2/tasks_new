﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using WordsDescription;

namespace TextBuilderConfigurator
{
    public class TextBuilder : ITextBuilder
    {
        private List<IWord> Words { get; set; }

        public TextBuilder()
        {
            Words = new List<IWord>();
        }

        public void AddWord(string wordname, int count, List<int> stringnumber)
        {
            Words.Add(new Word(wordname, count, stringnumber));
        }

        public string BuildText()
        {
            StringBuilder result = new StringBuilder();
            Words = Words.OrderBy(item => item.GetWordSelf()).ToList();
            List<IWord> temp = new List<IWord>();
            char capitalLetter;
            int i = 0;
            while (i < Words.Count)
            {
                capitalLetter = Words[i].GetWordSelf()[0];
                for (; i < Words.Count; i++)
                {
                    if (capitalLetter == Words[i].GetWordSelf()[0])
                    {
                        temp.Add(Words[i]);
                    }
                    else 
                    { 
                        break;
                    }                        
                }

                result.AppendLine($"{Char.ToUpper(capitalLetter)}");
                foreach (var line in temp)
                {
                    result.AppendLine(GetLine(line));
                }
                temp.Clear();
            }
            return result.ToString();
        }

        private string GetLine(IWord word)
        {
            StringBuilder line = new StringBuilder();
            foreach (var number in word.GetStringsNumber())
            {
                line.Append($"{number} ");
            }
            string tempString = line.ToString();
            line.Clear();
            line.Append($"....................................................{word.GetCount()}:{tempString}");
            line.Remove(0, word.GetWordSelf().Length).Insert(0, word.GetWordSelf());
            return line.ToString();
        }
    }

}