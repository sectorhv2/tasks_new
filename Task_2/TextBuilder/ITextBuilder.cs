﻿using System;
using System.Collections.Generic;

using WordsDescription;

namespace TextBuilderConfigurator
{
    public interface ITextBuilder
    {
        void AddWord(string wordname, int count, List<int> stringnumber);
        public string BuildText();
    }
}