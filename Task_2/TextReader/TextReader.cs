﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using InitialExceptions;

namespace TextReaderModul
{
    public class TextReader : ITextReader
    {
        public List<string> ReadFile(string path)
        {            
            try
            {
                return File.ReadAllLines(path).ToList();
            }
            catch (TextReaderException e)
            {
                Console.WriteLine($"Error: {e.Message}");
                throw;
            }              
        }
    }
}