﻿using System;
using System.Collections.Generic;

namespace TextReaderModul
{
    public interface ITextReader
    {
        List<string> ReadFile(string path);
    }
}