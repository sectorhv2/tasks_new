﻿using System;
using System.IO;
using InitialExceptions;
using TextBuilderConfigurator;

namespace TextWriterModul
{
    public class TextWriter : ITextWriter
    {
        public void WriteInFile(string path, ITextBuilder builder)
        {
            try 
            {
               File.WriteAllText(path, builder.BuildText());
            }
            catch (TextWriterException e)
            {
                Console.WriteLine($"Error: {e.Message}");
                throw;
            }
        }           
    }
}