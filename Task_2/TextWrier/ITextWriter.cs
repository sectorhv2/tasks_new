﻿using System;
using System.Collections.Generic;

using TextBuilderConfigurator;

namespace TextWriterModul
{
    public interface ITextWriter
    {
        void WriteInFile(string path, ITextBuilder builder);
    }
}