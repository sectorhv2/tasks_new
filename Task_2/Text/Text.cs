﻿using System;
using System.Collections.Generic;
using System.Linq;

using TextWriterModul;
using TextReaderModul;
using TextBuilderConfigurator;


namespace TextContainer
{
    class Text
    {        
        private readonly ITextWriter _writer;
        private readonly ITextReader _reader;
        private readonly ITextBuilder _builder;
        private readonly char[] markers = new char[] { '!', ' ', '?', '.', ',', ':', ';', '(', ')', '@' };        
        private readonly string Path;
        private List<string> WordsStrings;
        public Text(string path)
        {
            _reader = new TextReader();
            _writer = new TextWriter();
            _builder = new TextBuilder();
            Path = path;
        }

        public void CalculateResult(string pathOut)
        {
            GetListWords();
            FindeWordParams();
            _writer.WriteInFile(pathOut, _builder);
        }

        private void GetListWords()
        {
            List<string> listWords = _reader.ReadFile(Path);       
            WordsStrings = new List<string>();
            for (int i = 0; i < listWords.Count; i++)
            {
                WordsStrings.AddRange(listWords[i].ToLower().Split(markers, StringSplitOptions.RemoveEmptyEntries));
            }
        }

        private void FindeWordParams()
        {            
            for (int i = WordsStrings.Count - 1; i >= 0; i--)
            {
               _builder.AddWord(WordsStrings[i], WordsStrings.Count(x => x == WordsStrings[i]), FindLineNumbers(WordsStrings[i]));
               WordsStrings.RemoveAll(x => x == WordsStrings[i]);
               if (i != WordsStrings.Count)
                   i = WordsStrings.Count;
            }
        }

        private List<int> FindLineNumbers(string word)
        {
            List<string> listSentences = _reader.ReadFile(Path);            
            List<int> result = new List<int>();
            for (int i = 0; i < listSentences.Count; i++)
            {
                if (listSentences[i].ToLower().Split(markers).Contains(word))
                    result.Add(i+1);
            }
            return result;
        }       

    }
}
