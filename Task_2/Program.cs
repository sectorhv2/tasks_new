﻿using System;
using System.Configuration;
using TextContainer;

namespace ConcordanceTask_2
{
    static class Program
    {
        static void Main(string[] args)
        {            
            Text text = new Text(ConfigurationManager.AppSettings.Get("Adress Input"));
            text.CalculateResult(ConfigurationManager.AppSettings.Get("Adress Output"));            
        }
    }
}
