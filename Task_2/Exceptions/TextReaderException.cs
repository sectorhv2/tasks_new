﻿using System;
using System.IO;
using System.Runtime.Serialization;

namespace InitialExceptions
{
    [Serializable]
    public sealed class TextReaderException : FileNotFoundException
    {
        public TextReaderException (string message) : base(message)
        {
        }
        private TextReaderException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
