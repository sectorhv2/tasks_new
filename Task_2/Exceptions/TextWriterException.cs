﻿using System;
using System.IO;
using System.Runtime.Serialization;

namespace InitialExceptions
{
    [Serializable]
    public sealed class TextWriterException : IOException
    {
        public TextWriterException(string message) : base(message)
        {
        }
        private TextWriterException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
